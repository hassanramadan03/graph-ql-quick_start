const express = require('express');
const graphqlHTTP = require('express-graphql');
require('dotenv').config({silent:true})
const port=process.env.PORT;
const app = express();
 const MyGraphQLSchema=require('./schema')
app.use('/graphql', graphqlHTTP({
  schema: MyGraphQLSchema,
  graphiql: true
}));
 
app.listen(port,()=>{
    console.log(`server is running on http://localhost:${port}/graphql`,);
});