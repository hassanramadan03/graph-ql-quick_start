const {
    GraphQLObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLSchema
} = require('graphql')
const axios=require('axios')
const baseUrl='http://localhost:3000/customers'
const userType = new GraphQLObjectType({
    name: 'user',
    fields: {
        id:{type:GraphQLString} ,
        name: {type:GraphQLString},
        age: {type:GraphQLInt},
        email: {type:GraphQLString}
    }
})

const query = new GraphQLObjectType({
    name: 'rootQuery',
    fields: {
        user: {
            type: userType,
            args: {
                id: { type: GraphQLString }
            },
            resolve(parentValues, args) {
                 return axios.get(`${baseUrl}/${args.id}`)
                 .then(res=>res.data)
            }
        },
        users:{
            type: new GraphQLList(userType),
            resolve(perent,args){
                return axios.get(baseUrl)
                 .then(res=>res.data)
            }
        }
       
    }
})
const mutation=new GraphQLObjectType({
    name:'Mutaion',
    fields:{
        addUser:{
            type:userType,
            args:{
                name:{type:new GraphQLNonNull(GraphQLString)},
                email:{type:new GraphQLNonNull(GraphQLString)},
                age:{type:new GraphQLNonNull(GraphQLInt)},
            },
            resolve(parentValues,args){
                console.log(args);
                
                return axios.post(baseUrl,args).then(res=>res.data)
            }
        }
        ,
        updateUser:{
            type:userType,
            args:{
                id: {type:new GraphQLNonNull(GraphQLString)},
                name:{type:GraphQLString},
                email:{type:GraphQLString},
                age:{type:GraphQLInt},
            },
            resolve(parentValues,args){

                return axios.patch(`${baseUrl}/${args.id}`,args).then(res=>res.data)
            }
        },
        deleteUser:{
            type:userType,
            args:{
                id: {type:new GraphQLNonNull(GraphQLString)},
            },
            resolve(parentValues,args){
                return axios.delete(`${baseUrl}/${args.id}`).then(res=>res.data)
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query,
    mutation
})